import string
from random import choice

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

str_digits = '()+={}[]*/?-_' + string.ascii_letters + string.digits + string.ascii_uppercase
secret_key = ''.join(choice(str_digits) for i in range(50))

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@localhost/flask'
app.config['SECRET_KEY'] = secret_key
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
