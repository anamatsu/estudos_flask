from app import db

class User(db.Model):
    
    __tablename__ = 'users'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    
    
    def __init__(self, name):
        self.name = name
        
        
    def __str__(self):
        return self.name