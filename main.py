from flask import jsonify
from flask_migrate import Migrate

from app import app, db
from app.models import User

Migrate(app, db)


@app.shell_context_processor
def make_shell_context():
    return dict(
        app = app,
        db = db,
        user = User
    )
    
    
'''
    Comando do flask:
        flask db init => Inicia o banco de dados, somente quando o banco de dados for criado.
    
    Variáveis de ambiente do Flask:
        export FLASK_APP=main.py
        export FLASK_ENV=development
    
    Migrations:
        flask db migrate => Cria as migrações(tabelas)
        flask db upgrade => Cria as tabelas no banco de dados
'''